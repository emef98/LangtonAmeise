__Zum Team gehören:__ Leon Dienstbach; Eva-Maria Eifler

__Aufgabenverteilung:__

_Frontend: Leon Dienstbach_
* Aufbau des leeren Spielfelddesigns BoS
* Initialisierung der Ameise auf einem (beliebigen) Startfeld im BoS
und Positionsübergabe an das Backend
* Design und Darstellung der Ameise im BoS
* Animation der Ameise im BoS


_Backend: Eva-Maria Eifler_
* Interne Repräsentation des Spielfeldes
* Initialisierung und Verwaltung der Ameisen
* Implementieren der Programmlogik

_Allgemein:_
* Programmstart und Ablauf: Eva-Maria Eifler; Leon Dienstbach