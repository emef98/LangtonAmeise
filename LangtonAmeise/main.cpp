#include "stdafx.h"
int size = 120;
int board[120][120];

//Legt die Struktur einer Ameise fest und definiert ihren Typ
typedef struct{
	int ID; 
	int x;
	int y;
	int flag;
	int color;
	char viewDir; //Blickrichtung
}ant;

ant Ants[10]; // Initialisiert ein Array f�r die Ameisen
int colortable[10] = { YELLOW, RED, BLUE, GREEN, PINK, CYAN, ORANGE, FIREBRICK, LIME, SILVER };
char view[4] = { 'n','e','s','w' };
int sumAnt = 0;

//Randomisiert die Initialblickrichtung
int random(char arr[]) {
	return rand() % sizeof(arr);
}

//Legt die Ameisen im Array Ants an und speichert ihre Position
void initAnt(int x, int y) {
	Ants[sumAnt].ID = sumAnt;
	Ants[sumAnt].x = x;
	Ants[sumAnt].y = y;
	Ants[sumAnt].flag = 0;
	Ants[sumAnt].color = colortable[sumAnt];
	Ants[sumAnt].viewDir = view[random(view)];
	sumAnt++;
}

//F�rbt im die Ameisen im BoS
void faerben() {
	for (int i = 0; i <= sumAnt; i++) {
		farbe2(Ants[i].x, Ants[i].y, Ants[i].color);
		if (Ants[i].flag == 0) {
			farbe2(Ants[i].x, Ants[i].y, BLACK);
		}
		
	}
}

//Registriert nach einem Mausklick eine Ameise auf dem Feld
void click() { 
	if (sumAnt < 10) {
		int feld, x, y;
		char *a = abfragen();
		if (strlen(a) > 0) {
			printf("Nachricht: %s\n", a);
			if (a[0] == '#') {
				sscanf_s(a, "# %d %d %d", &feld, &x, &y);
				initAnt(x, y);
			}
		}
	}
}

//Beinhaltet den Algorithmus
void langton(int ID) {
	if (board[Ants[ID].x][Ants[ID].y] == 1) { //1. Auf einem gef�rbten (Flag 1) Feld 90 Grad nach links drehen
		switch (Ants[ID].viewDir) {
			case 'n': Ants[ID].viewDir = 'w'; break;
			case 'w': Ants[ID].viewDir = 's'; break;
			case 's': Ants[ID].viewDir = 'e'; break;
			case 'e': Ants[ID].viewDir = 'n'; break;
		}
		board[Ants[ID].x][Ants[ID].y] = 0; //2. Farbe wechseln
		Ants[ID].flag = 0;
	}
	else {
		switch (Ants[ID].viewDir) {//1. Auf einem ungef�rbten (Flag 0) Feld 90 Grad nach rechts drehen
		case 'n': Ants[ID].viewDir = 'e'; break;
		case 'e': Ants[ID].viewDir = 's'; break;
		case 's': Ants[ID].viewDir = 'w'; break;
		case 'w': Ants[ID].viewDir = 'n'; break;
		}
		board[Ants[ID].x][Ants[ID].y] = 1; // 2. Farbe wechseln
		Ants[ID].flag = 1;
	}
		switch (Ants[ID].viewDir) { // 3. Ein Feld in Blickrichtung bewegen mit Randkollisionsbehandlung
		case 'n':
			if (Ants[ID].y + 1 == size) {
				Ants[ID].y = 0; break;
			}
			else Ants[ID].y = Ants[ID].y + 1; break;

		case 'e':
			if (Ants[ID].x + 1 == size) {
				Ants[ID].x = 0; break;
			}
			else {
				Ants[ID].x = Ants[ID].x + 1; break;
			}
		case 'w':
			if (Ants[ID].x - 1 < 0) {
				Ants[ID].x = size-1; break;
			}
			else {
				Ants[ID].x = Ants[ID].x - 1; break;
			}
		case 's':
			if (Ants[ID].y - 1 < 0) {
				Ants[ID].y = size-1; break;
			}
			else {
				Ants[ID].y = Ants[ID].y - 1; break;
			}
		}
}

//Bewegt jede Ameise
void step() {//Bewegt die Ameisen und liefert jeweils die aktuallisierte Ameise
	for (int i = 0; i <= sumAnt; i++) {
		langton(i);
	}
}

int _tmain(int argc, _TCHAR* argv[]){
	// Initialisiert das Spielfeld
	loeschen();
	groesse(size, size);
	flaeche(BLACK);
	formen("d");
	farben(BLACK);
	rahmen(WHITE);
	
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			board[i][j] = 0;
		}
	}
	// Programmablauf
	for (;;) {
		click();
		faerben();
		step();
	}
}