__Langton Ameise__

![Programmgrafik](https://git.thm.de/emef98/LangtonAmeise/raw/master/langton.jpg)

Das Projekt zur Langton Ameise bildet einen von Christopher Langton entwickelten Algorithmus
(https://de.wikipedia.org/wiki/Ameise_%28Turingmaschine%29) nach.

Daf�r sind folgende 3 Anweisungen impelmentiert:

__1.__ 	Auf einem ungef�rbten Feld drehe 90 Grad nach rechts; <br/>
		auf einem gef�rbten Feld drehe 90 Grad nach links.

__2.__	Wechsle die Farbe des Feldes (ungef�rbt nach gef�rbt, gef�rbt nach ungef�rbt)

__3.__	Schreite ein Feld in der aktuellen Blickrichtung fort.

Es besteht die M�glichkeit bis zu zehn Ameisen auf dem 120x120 Pixel gro�en Spielfeld anzulegen.
Durch klicken auf das leere, initialisierte Spielfeld wird eine neue Ameise an der Position
generiert. Jede Ameise hat eine eigene Farbe und bei der Generierung eine zuf�llig ausgew�hlte
Blickrichtung und handelt anschlie�en nach den oben erl�uterten 3 Anweisungsschritten.

Das Programm wurde f�r die Benutzeroberfl�che BoS entwickelt: __https://github.com/stephaneuler/board-of-symbols__

Autoren: __Leon Dienstbach; Eva-Maria Eifler__