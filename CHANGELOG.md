`[UPD][09.01.2016][Eva-Maria Eifler]` Abschließendes Update der README-Datei, der CONTRIBUTING-Datei und der CHANGELOG-Datei<br/>
<br/>
`[UPD][08.01.2016][Eva-Maria Eifler]` Ordnen des Git-Repors und überarbeiten der Gitignore<br/>
`[UPD][08.01.2016][Eva-Maria Eifler]` Refactoring des Codes via Ausdünnung und Zusammenlegung einzelner Elemente<br/>
`[NEW][08.01.2016][Eva-Maria Eifler]` Die Blickrichtung der Ameise erfolgt jetzt zufällig über die random (char arr[]) Funktion<br/>
`[UPD][08.01.2016][Leon Dienstbach]` Darstellungsform verworfen, da zu klein auf der Spielfeldgröße - jetzt: Diamantdastellung<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Dastellung der Ameisen ja nach Blickrichtung in entsprechende Dreiecke angepasst<br/>
`[UPD][08.01.2016][Leon Dienstbach]` Code entschlackt<br/>
`[NEW][08.01.2016][Eva-Maria Eifler]` Begrenzung der Anzahl der Ameisen auf 10<br/>
`[FIX][08.01.2016][Eva-Maria Eifler]` Der Methode langton() - Randkollisionsbehandlung abschließend repariert<br/>
`[UPD][08.01.2016][Eva-Maria Eifler]` Umbenennung der Methode "turing" in "langton"<br/>
`[UPD][08.01.2016][Leon Dienstbach]` Der Methode turing(int ID) um die flag der Ameise<br/>
`[UPD][08.01.2016][Leon Dienstbach]` Ergänzung der Ameise um die Eigenschaft Flag zur Repräsentation des aktuellen Feldes im BoS<br/>
`[FIX][08.01.2016][Leon Dienstbach]` Fehler in der Randkollisionsbehandlung der Methode turing(int ID)<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Methode click() zur Registierung einer Ameise<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Methode faerben() zur Darstellung und Animation der Ameisen im BoS<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Methode design() zur Repräsentation des Designs<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Farbauswahl für das BoS getroffen<br/>
`[NEW][08.01.2016][Leon Dienstbach]` Designstruktur entwickelt<br/>
`[UPD][08.01.2016][Leon Dienstbach]` Struktur der Ameise erweitert<br/>
<br/>
`[FIX][07.01.2016][Eva-Maria Eifler]` Switch-Case in der Methode "step()" um "break" ergänzt<br/>
`[NEW][07.01.2016][Eva-Maria Eifler]` Dokumentation <br/>
`[FIX][07.01.2016][Eva-Maria Eifler]` Überholte Strukturen gelöscht<br/>
`[FIX][07.01.2016][Eva-Maria Eifler]` Korrektur der README-Datei<br/>
`[UPD][07.01.2016][Eva-Maria Eifler]` Update der CHANGELOG-Datei<br/>
`[NEW][07.01.2016][Eva-Maria Eifler]` Methode turing(int ID) mit dem Langtonalgorithmus<br/>
`[NEW][07.01.2016][Eva-Maria Eifler]` Methode "step()" zur Bewegung und Aktualisierung der Ameise<br/>
`[NEW][07.01.2016][Eva-Maria Eifler]` Speicherung der Ameisen festlegt<br/>
`[NEW][07.01.2016][Eva-Maria Eifler]` Struktur der Ameisen festlegt<br/>
<br/>
`[NEW][17.12.2015][Eva-Maria Eifler]` CONTRIBUTING angelegt<br/>
`[NEW][17.12.2015][Eva-Maria Eifler]` CHANGELOG angelegt<br/>
`[NEW][17.12.2015][Eva-Maria Eifler]` README angelegt<br/>

__Legende__<br/>
`[NEW]` - neu<br/>
`[FIX]` - repariert<br/>
`[UPD]` - geupdatet
